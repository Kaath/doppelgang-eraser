```
 a88888b. M""MMMMM""M                     dP   dP                
d8'   `88 M  MMMM' .M                     88   88                
88 d8P 88 M       .MM .d8888b. .d8888b. d8888P 88d888b. .d8888b. 
88 Yo8b88 M  MMMb. YM 88'  `88 88'  `88   88   88'  `88 88ooood8 
Y8.       M  MMMMb  M 88.  .88 88.  .88   88   88    88 88.  ... 
 Y88888P' M  MMMMM  M `88888P8 `88888P8   dP   dP    dP `88888P' 
          MMMMMMMMMMM                                            
```

# Dopplgang-eraser

_**Description:**_ The purpose of this small command-line utility is to providea quick and easy way to find and delete files in addition to a copy on its machine to save storage space.

# Installation guide

You need to have go installed to compile this program.

You can find how to install it [here](https://golang.org/dl/)

### Command lines for Linux

```
git clone git@gitlab.com:Kaath/doppelgang-eraser.git
cd doppelgang-eraser
sudo make install
```

### Command lines for MacOS

If you don't have homebrew installed install it with the following lines

```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

You also need make

```
brew install make --with-default-names
```

Then copy and paste those lines in a terminal

```
git clone git@gitlab.com:Kaath/doppelgang-eraser.git
cd doppelgang-eraser
sudo make install
```

or click on the link to download the [zip](https://gitlab.com/Kaath/doppelgang-eraser/-/archive/master/doppelgang-eraser-master.zip)
unzip > go inside the folder > type the following into your console:

```
sudo make install
```

### Command lines for Windows

yen a pas sry (come back soon)

# User Guide

doppelgang-eraser [ -b | -d | -r | -output=_path_ ] path
