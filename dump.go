package main

import (
	"fmt"
	"io"
)

func BulkPrint(duplicates []PathGroup, iostream io.Writer) {
	fmt.Fprintln(iostream, "======== originals ========")

	for _, pg := range duplicates {
		fmt.Fprintln(iostream, "======= "+pg.OriginPath+" =======")
		fmt.Fprintln(iostream, "")

		for _, files := range pg.HashMap {
			fmt.Fprintln(iostream, files[0].Name)
		}

		fmt.Fprintln(iostream, "")

		fmt.Fprintln(iostream, "======== duplicates ========")

		for _, files := range pg.HashMap {
			for i := 1; i < len(files); i++ {
				fmt.Fprintln(iostream, files[i].Name)
			}
		}
	}
}

func DetailedPrint(duplicates []PathGroup) {
	fmt.Println("======== duplicates ========")
	fmt.Println()

	for _, pg := range duplicates {
		fmt.Println("======= " + pg.OriginPath + " =======")
		fmt.Println()
		for _, files := range pg.HashMap {
			fmt.Println("======== " + files[0].Name + " ========")
			for i := 1; i < len(files); i++ {
				fmt.Println(files[i])
			}
		}
	}
}
