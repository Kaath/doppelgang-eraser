package main

import (
	"fmt"
	"io"
	"log"
	"os"
)

func trim(path string) string {
	for path[len(path)-1] == '/' {
		path = path[:len(path)-1]
	}
	return path
}

func contains(array []*os.File, elt *os.File) bool {
	for _, file := range array {
		if CompareFiles(file, elt) {
			return true
		}
	}
	return false
}

func longest(strs []TimeStampedFile) (max int) {
	for _, str := range strs {
		if max < len(str.Name) {
			max = len(str.Name)
		}
	}
	return max
}

func printErr(err error, opt Options) {
	if opt.Verbose {
		var file io.Writer
		if opt.Output != "" {
			var err error
			file, err = os.OpenFile(opt.Output,
				os.O_APPEND|os.O_CREATE|os.O_RDWR,
				0644)
			if err != nil {
				log.Fatal(err)
			}
		} else {
			file = os.Stderr
		}
		fmt.Fprintln(file, err)
	}
}
