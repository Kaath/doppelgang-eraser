package main

import (
	"regexp"
	"strings"
)

type Options struct {
	Bulk     bool
	Detailed bool
	Autodel  bool
	Verbose  bool
	Cli      bool
	Output   string
	Paths    []string
}

func ParseOptions(args []string) Options {
	var rOutput = regexp.MustCompile("^-output=")
	var opt Options
	if len(args) > 1 {
		for _, value := range args[1:] {
			if rOutput.MatchString(value) {
				index := strings.Index(value, "=")
				opt.Output = value[index+1:]
				continue
			}

			switch value {
			case "-b":
				opt.Bulk = true
				opt.Detailed = false
			case "-d":
				opt.Detailed = true
				opt.Bulk = false
			case "-r":
				opt.Autodel = true
			case "-v":
				opt.Verbose = true
			case "--cli":
			case "-c":
				opt.Cli = true
			default:
				opt.Paths = append(opt.Paths, value)
			}
		}
	}

	if opt.Paths == nil {
		opt.Paths = []string{"."}
	}

	return opt
}
