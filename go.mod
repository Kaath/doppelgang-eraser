module doppelgang-eraser

require (
	github.com/aarzilli/nucular v0.0.0-20190708141646-0996317e0153
	github.com/atotto/clipboard v0.1.2
	github.com/mattn/go-runewidth v0.0.4 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/nsf/termbox-go v0.0.0-20190624072549-eeb6cd0a1762
	golang.org/x/mobile v0.0.0-20190318164015-6bd122906c08
)
