package main

import (
	"fmt"
	"github.com/aarzilli/nucular"
	"github.com/atotto/clipboard"
	"golang.org/x/mobile/event/key"
	"github.com/mitchellh/go-homedir"
	"os"
	"strconv"
	"time"
)

var doppleGUI GUI

type GUI struct {
	te         nucular.TextEditor
	opt        Options
	originPath string
	selected   []bool
	paths      []TimeStampedFile
	duplicate  []PathGroup
}

func CreateGUI(options Options) GUI {
	var gui GUI
	gui.te.Flags = nucular.EditSimple
	gui.opt = options

	return gui
}

func containsString(array []string, value string) bool {
	for _, val := range array {
		if val == value {
			return true
		}
	}
	return false
}

func removeBoolAt(slice []bool, s int) []bool {
	return append(slice[:s], slice[s+1:]...)
}

func removeTsfAt(slice []TimeStampedFile, s int) []TimeStampedFile {
	return append(slice[:s], slice[s+1:]...)
}

func Updatefn(w *nucular.Window) {
	w.Row(40).Dynamic(1)
	doppleGUI.te.Edit(w)

	inputs := w.KeyboardOnHover(w.Bounds)
	if inputs.Pressed(key.CodeLeftControl) && inputs.Pressed(key.CodeV) {
		fmt.Println("print shit")
		str, _ := clipboard.ReadAll()
		doppleGUI.te.Buffer = []rune(str)
	}

	w.Row(60).Dynamic(3)
	if w.ButtonText("Launch") {
		fmt.Println("launched shit")
		var duplicates []PathGroup
		path := string(doppleGUI.te.Buffer)
		path, _ = homedir.Expand(path)

		hmap := AnalyzeDir(path, doppleGUI.opt)
		RemoveSolos(&hmap)
		pg := PathGroup{path, hmap}
		duplicates = append(duplicates, pg)

		doppleGUI.duplicate = duplicates
		fmt.Println(doppleGUI.duplicate)

		for _, pg := range doppleGUI.duplicate {
			for _, array := range pg.HashMap {
				for _, tsf := range array {
					doppleGUI.paths = append(doppleGUI.paths, tsf)
					doppleGUI.selected = append(doppleGUI.selected, false)
				}
				doppleGUI.paths = append(doppleGUI.paths, TimeStampedFile{"", time.Now()})
				doppleGUI.selected = append(doppleGUI.selected, false)
			}
		}
	}
	if w.ButtonText("Remove") {
		removes := []int{}
		for index, boolean := range doppleGUI.selected {
			if boolean {
				os.Remove(doppleGUI.paths[index].Name)
				removes = append(removes, index)
			}
		}
		for index := len(removes) - 1; index >= 0; index-- {
			doppleGUI.selected = removeBoolAt(doppleGUI.selected, removes[index])
			doppleGUI.paths = removeTsfAt(doppleGUI.paths, removes[index])
		}
	}
	if w.ButtonText("Home") {
		hd, _ := homedir.Dir()
		doppleGUI.te.Buffer = append(doppleGUI.te.Buffer, []rune(hd)...)
	}


	if doppleGUI.duplicate != nil {
		count := 1
		for index := 0; index < len(doppleGUI.paths); index++ {
			number := index
			for doppleGUI.paths[index].Name != "" {
				index++
			}
			number = index - number
			index = index - number
			if number == 0 {
				continue
			}
			tsf := doppleGUI.paths[index]
			w.Row(20).Dynamic(1)
			if w.TreePush(nucular.TreeTab, "batch " + strconv.Itoa(count) + " - " + strconv.Itoa(number) + " duplicates", false) {
				for tsf.Name != "" {
					w.Row(20).Dynamic(1)
					w.CheckboxText(tsf.Name+" "+tsf.Timestamp.Format("2 Jan 2006 15:04:05"),
						&(doppleGUI.selected[index]))
					index++
					tsf = doppleGUI.paths[index]
				}
				w.TreePop()
			} else {
				for doppleGUI.paths[index].Name != "" {
					index++
				}
			}
			count++
		}
	}
}
