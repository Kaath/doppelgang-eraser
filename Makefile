dopple:
	go build -race -ldflags "-extldflags '-static'" -o dopple

install: dopple
	mv dopple /usr/bin

clean:
	rm -f dopple doppelgang-eraser

.PHONY: dopple install clean
