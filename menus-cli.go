package main

import (
	"fmt"
	"github.com/nsf/termbox-go"
	"os"
)

type Menu struct {
	NbElements int
	CursorPos  int
	Options    []TimeStampedFile
	Selected   []bool
}

func CreateMenus(pg PathGroup) (menuList []Menu) {
	for _, value := range pg.HashMap {
		tmp := Menu{len(value), 0, value, make([]bool, len(value))}
		menuList = append(menuList, tmp)
	}
	return menuList
}

func printMenu(menu Menu) {
	fmt.Print("Which do you want to delete ?\n\n")
	sortTimeStamped(&menu.Options)
	for i := 0; i < menu.NbElements; i++ {
		var arrow string
		var checked string
		if i == menu.CursorPos {
			arrow = "->"
		} else {
			arrow = "  "
		}
		if menu.Selected[i] {
			checked = "x"
		} else {
			checked = " "
		}

		timestamp := menu.Options[i].Timestamp.Format("2 Jan 2006 15:04:05")
		fmt.Printf("| %s %-*s - %s [%s]\n",
			arrow,
			longest(menu.Options),
			menu.Options[i].Name,
			timestamp,
			checked)
	}
}

func controlMenu(menu Menu) (delList []TimeStampedFile) {
	var run bool = true
	CallClear()
	printMenu(menu)
	for run {
		switch ev := termbox.PollEvent(); ev.Type {
		case termbox.EventKey:
			switch ev.Key {
			case termbox.KeyEsc:
				for index, value := range menu.Selected {
					if value {
						menu.Selected[index] = false
					}
				}
				run = false
			case termbox.KeyEnter:
				run = false
			case termbox.KeySpace:
				val := menu.Selected[menu.CursorPos]
				menu.Selected[menu.CursorPos] = !val
			case termbox.KeyArrowUp:
				if menu.CursorPos > 0 {
					menu.CursorPos--
				}
			case termbox.KeyArrowDown:
				if menu.CursorPos < menu.NbElements-1 {
					menu.CursorPos++
				}
			default:
				continue
			}
		case termbox.EventError:
			panic(ev.Err)
		}
		CallClear()
		printMenu(menu)
	}
	for index, str := range menu.Options {
		if menu.Selected[index] {
			delList = append(delList, str)
		}
	}
	return delList
}

func controlMenus(menus []Menu) (delList []TimeStampedFile) {
	for _, menu := range menus {
		list := controlMenu(menu)
		delList = append(delList, list...)
	}
	return delList
}

func InteractiveDel(dup []PathGroup) {
	for _, pg := range dup {
		menuList := CreateMenus(pg)
		delList := controlMenus(menuList)
		for _, str := range delList {
			os.Remove(str.Name)
		}
	}
}
