package main

import (
	"io"
	"log"
	"os"

	"github.com/aarzilli/nucular"
	"github.com/aarzilli/nucular/style"
	"github.com/nsf/termbox-go"
)

func main() {
	var opt Options = ParseOptions(os.Args)
	if !opt.Cli {
		wnd := nucular.NewMasterWindow(0, "Title", Updatefn)
		wnd.SetStyle(style.FromTheme(style.DarkTheme, 1.0))
		wnd.Main()
	} else {
		err := termbox.Init()
		if err != nil {
			panic(err)
		}
		defer termbox.Close()

		var duplicates []PathGroup
		for _, path := range opt.Paths {
			hmap := AnalyzeDir(path, opt)
			RemoveSolos(&hmap)
			pg := PathGroup{path, hmap}
			duplicates = append(duplicates, pg)
		}

		var file io.Writer
		if opt.Output != "" {
			var err error
			file, err = os.OpenFile(opt.Output,
				os.O_APPEND|os.O_CREATE|os.O_RDWR,
				0644)
			if err != nil {
				log.Fatal(err)
			}
		} else {
			file = os.Stdout
		}

		if opt.Bulk {
			BulkPrint(duplicates, file)
		} else if opt.Detailed {
			DetailedPrint(duplicates)
		} else {
			InteractiveDel(duplicates)
		}
	}
}
