package main

import (
	"bytes"
	"io"
	"log"
	"os"
	"strings"
	"testing"
	"time"

)

func createFile(pathname string) {
	file, err := os.Create(pathname + "dummy.tt")
	file2, err2 := os.Create(pathname + "dummy2.tt")

	if err != nil {
		log.Fatal(err)
	}
	if err2 != nil {
		log.Fatal(err2)
	}

	file.Write([]byte("Ceci est un contenu"))
	file2.Write([]byte("Ceci est un contenu"))
}

func createDir(depth int) {
	if depth == 0 {
		return
	}

	var path []string

	for i := 0; i < depth; i++ {
		path = append(path, "dir")
	}

	os.MkdirAll(strings.Join(path, "/"), os.ModePerm)
}

func removeFile() {
	os.Remove("dummy.tt")
	os.Remove("dummy2.tt")
}

func removeDir() {
	os.RemoveAll("dir")
}

func containsName(array map[string][]TimeStampedFile, str string) (bool, int) {
	for _, duplicates := range array {
		for index, file := range duplicates {
			if file.Name == str {
				return true, index
			}
		}
	}
	return false, 0
}

func StringEquals(array1 []TimeStampedFile, array2 []TimeStampedFile) bool {
	if len(array1) != len(array2) {
		return false
	} else {
		for index, value := range array1 {
			if value.Name != array2[index].Name {
				return false
			}
		}
		return true
	}
}

func BoolEquals(array1 []bool, array2 []bool) bool {
	if len(array1) != len(array2) {
		return false
	} else {
		for index, value := range array1 {
			if value != array2[index] {
				return false
			}
		}
		return true
	}
}

func TestCompareHashSame(t *testing.T) {
	createFile("")

	file, err := os.Open("dummy.tt")
	if err != nil {
		log.Fatal(err)
	}

	file2, err := os.Open("dummy2.tt")

	if err != nil {
		log.Fatal(err)
	}

	if !CompareFiles(file, file2) {
		t.Errorf("Compared similar files: Expected True got False")
	}

	removeFile()
}

func TestCompareHashDifferent(t *testing.T) {
	createFile("")

	file, err := os.OpenFile("dummy.tt",
		os.O_APPEND|os.O_CREATE|os.O_RDWR,
		0644)
	if err != nil {
		log.Fatal(err)
	}

	_, err_write := file.WriteString("fuck")
	if err_write != nil {
		log.Fatal(err_write)
	}

	file2, err := os.Open("dummy2.tt")

	if err != nil {
		log.Fatal(err)
	}

	if CompareFiles(file, file2) {
		t.Errorf("Compared similar files: Expected False got True")
	}

	removeFile()
}

func TestParseNoOptions(t *testing.T) {
	var opt Options = ParseOptions([]string{})

	if opt.Bulk == true {
		t.Errorf("ParsingNoOptions: Bulk should be at false")
	}
	if opt.Detailed == true {
		t.Errorf("ParsingNoOptions: Detailed should be at false")
	}
	if opt.Autodel == true {
		t.Errorf("ParsingNoOptions: Autodel should be at false")
	}
	if opt.Verbose == true {
		t.Errorf("ParsingNoOptions: remove should be at false")
	}
	if opt.Output != "" {
		t.Errorf("ParsingNoOptions: Output should be at \"\"")
	}
	if len(opt.Paths) != 1 {
		t.Errorf("ParsingNoOptions: more Paths than one")
	}
	if opt.Paths[0] != "." {
		t.Errorf("ParsingNoOptions: expected \".\" got %s", opt.Paths[0])
	}
}

func TestParseBulkOptions(t *testing.T) {
	var opt Options = ParseOptions([]string{"prog", "-b"})

	if opt.Bulk == false {
		t.Errorf("ParsingBulkOptions: Bulk should be at true")
	}
	if opt.Detailed == true {
		t.Errorf("ParsingBulkOptions: Detailed should be at false")
	}
	if opt.Autodel == true {
		t.Errorf("ParsingBulkOptions: Autodel should be at false")
	}
	if opt.Verbose == true {
		t.Errorf("ParsingBulkOptions: remove should be at false")
	}
	if opt.Output != "" {
		t.Errorf("ParsingBulkOptions: Output should be at \"\"")
	}
	if len(opt.Paths) != 1 {
		t.Errorf("ParsingBulkOptions: more Paths than one")
	}
	if opt.Paths[0] != "." {
		t.Errorf("ParsingBulkOptions: expected \".\" got %s", opt.Paths[0])
	}
}

func TestParseDetailedOptions(t *testing.T) {
	var opt Options = ParseOptions([]string{"prog", "-d"})

	if opt.Bulk == true {
		t.Errorf("ParsingDetailedOptions: Bulk should be at false")
	}
	if opt.Detailed == false {
		t.Errorf("ParsingDetailedOptions: Detailed should be at true")
	}
	if opt.Autodel == true {
		t.Errorf("ParsingDetailedOptions: Autodel should be at false")
	}
	if opt.Verbose == true {
		t.Errorf("ParsingDetailedOptions: remove should be at false")
	}
	if opt.Output != "" {
		t.Errorf("ParsingDetailedOptions: Output should be at \"\"")
	}
	if len(opt.Paths) != 1 {
		t.Errorf("ParsingDetailedOptions: more Paths than one")
	}
	if opt.Paths[0] != "." {
		t.Errorf("ParsingDetailedOptions: expected \".\" got %s", opt.Paths[0])
	}
}

func TestParseBothOptions(t *testing.T) {
	var opt Options = ParseOptions([]string{"prog", "-d", "-b"})

	if opt.Bulk == false {
		t.Errorf("ParsingBothOptions: Bulk should be at true")
	}
	if opt.Detailed == true {
		t.Errorf("ParsingBothOptions: Detailed should be at false")
	}
	if opt.Autodel == true {
		t.Errorf("ParsingBothOptions: Autodel should be at false")
	}
	if opt.Verbose == true {
		t.Errorf("ParsingBothOptions: remove should be at false")
	}
	if opt.Output != "" {
		t.Errorf("ParsingBothOptions: Output should be at \"\"")
	}
	if len(opt.Paths) != 1 {
		t.Errorf("ParsingBothOptions: more Paths than one")
	}
	if opt.Paths[0] != "." {
		t.Errorf("ParsingBothOptions: expected \".\" got %s", opt.Paths[0])
	}
}

func TestParseRemove(t *testing.T) {
	var opt Options = ParseOptions([]string{"prog", "-r"})

	if opt.Bulk == true {
		t.Errorf("ParsingRemove: Bulk should be at true")
	}
	if opt.Detailed == true {
		t.Errorf("ParsingRemove: Detailed should be at false")
	}
	if opt.Autodel == false {
		t.Errorf("ParsingRemove: Autodel should be at true")
	}
	if opt.Verbose == true {
		t.Errorf("ParsingRemove: remove should be at false")
	}
	if opt.Output != "" {
		t.Errorf("ParsingRemove: Output should be at \"\"")
	}
	if len(opt.Paths) != 1 {
		t.Errorf("ParsingRemove: more Paths than one")
	}
	if opt.Paths[0] != "." {
		t.Errorf("ParsingRemove: expected \".\" got %s", opt.Paths[0])
	}
}

func TestParseVerbose(t *testing.T) {
	var opt Options = ParseOptions([]string{"prog", "-v"})

	if opt.Bulk == true {
		t.Errorf("ParsingVerbose: Bulk should be at true")
	}
	if opt.Detailed == true {
		t.Errorf("ParsingVerbose: Detailed should be at false")
	}
	if opt.Autodel == true {
		t.Errorf("ParsingVerbose: Autodel should be at false")
	}
	if opt.Verbose == false {
		t.Errorf("ParsingVerbose: Verbose should be at true")
	}
	if opt.Output != "" {
		t.Errorf("ParsingVerbose: Output should be at \"\"")
	}
	if len(opt.Paths) != 1 {
		t.Errorf("ParsingVerbose: more Paths than one")
	}
	if opt.Paths[0] != "." {
		t.Errorf("ParsingVerbose: expected \".\" got %s", opt.Paths[0])
	}
}

func TestParseOutput(t *testing.T) {
	var opt Options = ParseOptions([]string{"prog", "-output=shit"})

	if opt.Bulk == true {
		t.Errorf("ParsingOutput: Bulk should be at false")
	}
	if opt.Detailed == true {
		t.Errorf("ParsingOutput: Detailed should be at false")
	}
	if opt.Autodel == true {
		t.Errorf("ParsingOutput: Autodel should be at false")
	}
	if opt.Verbose == true {
		t.Errorf("ParsingOutput: Verbose should be at false")
	}
	if opt.Output != "shit" {
		t.Errorf("ParsingOutput: Output should be at \"shit\"")
	}
	if len(opt.Paths) != 1 {
		t.Errorf("ParsingOutput: more Paths than one")
	}
	if opt.Paths[0] != "." {
		t.Errorf("ParsingOutput: expected \".\" got %s", opt.Paths[0])
	}
}

func TestParseOutputMultiple(t *testing.T) {
	var opt Options = ParseOptions([]string{"prog", "-output=shit", "-output=caca"})

	if opt.Bulk == true {
		t.Errorf("ParsingOutputMultiple: Bulk should be at true")
	}
	if opt.Detailed == true {
		t.Errorf("ParsingOutputMultiple: Detailed should be at false")
	}
	if opt.Autodel == true {
		t.Errorf("ParsingOutputMultiple: Autodel should be at false")
	}
	if opt.Verbose == true {
		t.Errorf("ParsingOutputMultiple: Verbose should be at false")
	}
	if opt.Output != "caca" {
		t.Errorf("ParsingOutputMultiple: Output should be at \"caca\"")
	}
	if len(opt.Paths) != 1 {
		t.Errorf("ParsingOutputMultiple: more Paths than one")
	}
	if opt.Paths[0] != "." {
		t.Errorf("ParsingOutputMultiple: expected \".\" got %s", opt.Paths[0])
	}
}

func TestParsePath(t *testing.T) {
	var opt Options = ParseOptions([]string{"prog", "shit"})

	if opt.Bulk == true {
		t.Errorf("ParsingPath: Bulk should be at false")
	}
	if opt.Detailed == true {
		t.Errorf("ParsingPath: Detailed should be at false")
	}
	if opt.Autodel == true {
		t.Errorf("ParsingPath: Autodel should be at false")
	}
	if opt.Verbose == true {
		t.Errorf("ParsingPath: Verbose should be at false")
	}
	if opt.Output != "" {
		t.Errorf("ParsingPath: Output should be at \"\"")
	}
	if len(opt.Paths) != 1 {
		t.Errorf("ParsingPath: more Paths than one")
	}
	if opt.Paths[0] != "shit" {
		t.Errorf("ParsingPath: expected \"shit\" got \"%s\"", opt.Paths[0])
	}
}

func TestParsePathMultiple(t *testing.T) {
	var opt Options = ParseOptions([]string{"prog", "shit", "caca"})

	if opt.Bulk == true {
		t.Errorf("ParsingPathMultiple: Bulk should be at false")
	}
	if opt.Detailed == true {
		t.Errorf("ParsingPathMultiple: Detailed should be at false")
	}
	if opt.Autodel == true {
		t.Errorf("ParsingPathMultiple: Autodel should be at false")
	}
	if opt.Verbose == true {
		t.Errorf("ParsingPathMultiple: Verbose should be at false")
	}
	if opt.Output != "" {
		t.Errorf("ParsingPathMultiple: Output should be at \"\"")
	}
	if len(opt.Paths) != 2 {
		t.Errorf("ParsingPathMultiple: more Paths than two")
	}
	if opt.Paths[0] != "shit" {
		t.Errorf("ParsingPathMultiple: expected \"shit\" got \"%s\"", opt.Paths[0])
	}
	if opt.Paths[1] != "caca" {
		t.Errorf("ParsingPathMultiple: expected \"caca\" got \"%s\"", opt.Paths[1])
	}
}

func TestCompareHashEmpty(t *testing.T) {
	createFile("")

	file, err := os.Create("dummy3.tt")
	if err != nil {
		log.Fatal(err)
	}

	file2, err := os.Open("dummy2.tt")

	if err != nil {
		log.Fatal(err)
	}

	if CompareFiles(file, file2) {
		t.Errorf("Compared similar files: Expected False got True")
	}

	os.Remove("dummy3.tt")

	removeFile()
}

func TestRemoveSolos(t *testing.T) {
	maps := make(map[string][]TimeStampedFile)

	maps["a"] = []TimeStampedFile{{"a1", time.Now()}, {"a2", time.Now()}}
	maps["b"] = []TimeStampedFile{{"b1", time.Now()}}

	RemoveSolos(&maps)

	if maps["b"] != nil {
		t.Errorf("removeSolos: didn't remove the solo")
	}
}

func TestCompareCurrentDirectory(t *testing.T) {
	createFile("")

	var opt Options

	duplicate := AnalyzeDir(".", opt)

	if is_there, _ := containsName(duplicate, "./dummy.tt"); !is_there {
		t.Errorf("original: dummy.tt not there")
	}

	if is_there, _ := containsName(duplicate, "./dummy2.tt"); !is_there {
		t.Errorf("duplicate: dummy2.tt not there")
	}

	removeFile()
}

func TestCompareDirectory(t *testing.T) {
	createDir(1)
	createFile("dir/")

	var opt Options

	duplicate := AnalyzeDir("dir", opt)

	if is_there, where := containsName(duplicate, "dir/dummy.tt"); !is_there || where != 0 {
		t.Errorf("original: dir/dummy.tt not there")
	}

	if is_there, where := containsName(duplicate, "dir/dummy2.tt"); !is_there || where != 1 {
		t.Errorf("duplicate: dir/dummy2.tt not there")
	}

	removeDir()
}

func TestCompareAcrossDirectory(t *testing.T) {
	createDir(2)
	createFile("dir/")

	file, err := os.Create("dir/dir/dummy3.tt")

	if err != nil {
		log.Fatal(err)
	}

	file.Write([]byte("Ceci est un contenu"))

	var opt Options

	duplicate := AnalyzeDir("dir", opt)

	if is_there, _ := containsName(duplicate, "dir/dummy.tt"); !is_there {
		t.Errorf("original: dir/dummy.tt not there")
	}

	if is_there, _ := containsName(duplicate, "dir/dummy2.tt"); !is_there {
		t.Errorf("duplicate: dir/dummy2.tt not there")
	}

	if is_there, _ := containsName(duplicate, "dir/dir/dummy3.tt"); !is_there {
		t.Errorf("duplicate: dir/dummy2.tt not there")
	}

	removeDir()
}

func TestCompareDirectoryNestedOnce(t *testing.T) {
	createDir(2)
	createFile("dir/dir/")
	createFile("dir/")

	var opt Options

	duplicate := AnalyzeDir("dir", opt)

	if is_there, where := containsName(duplicate, "dir/dir/dummy.tt"); !is_there || where != 0 {
		t.Errorf("duplicate: did not get dir/dir/dummy.tt")
	}

	if is_there, where := containsName(duplicate, "dir/dummy.tt"); !is_there || where == 0 {
		t.Errorf("duplicate: did not get dir/dummy.tt")
	}

	if is_there, where := containsName(duplicate, "dir/dir/dummy2.tt"); !is_there || where == 0 {
		t.Errorf("duplicate: did not get dir/dir/dummy2.tt")
	}

	if is_there, where := containsName(duplicate, "dir/dummy2.tt"); !is_there || where == 0 {
		t.Errorf("duplicate: did not get dir/dummy2.tt")
	}

	removeDir()
}

func TestBulkPrint(t *testing.T) {
	createDir(1)
	createFile("dir/")

	var opt Options

	duplicates := PathGroup{"dir", AnalyzeDir("dir", opt)}

	file, err := os.OpenFile("tmp_dump",
		os.O_APPEND|os.O_CREATE|os.O_RDWR,
		0644)
	if err != nil {
		log.Fatal(err)
	}

	BulkPrint([]PathGroup{duplicates}, file)
	buf := bytes.NewBuffer(nil)

	file.Seek(0, os.SEEK_SET)
	io.Copy(buf, file)
	file.Close()

	var str strings.Builder
	str.WriteString("======== originals ========\n")
	str.WriteString("======= dir =======\n")
	str.WriteByte('\n')
	str.WriteString("dir/dummy.tt\n")
	str.WriteByte('\n')
	str.WriteString("======== duplicates ========\n")
	str.WriteString("dir/dummy2.tt\n")

	if buf.String() != str.String() {
		t.Errorf("BulkPrint: got %s\nnot\n%s", buf.String(), str.String())
	}

	os.Remove("tmp_dump")
	removeFile()
}

func TestBulkPrintNestedOnce(t *testing.T) {
	createDir(2)
	createFile("dir/")
	createFile("dir/dir/")

	var opt Options

	duplicates := PathGroup{"dir", AnalyzeDir("dir", opt)}

	file, err := os.OpenFile("tmp_dump",
		os.O_APPEND|os.O_CREATE|os.O_RDWR,
		0644)
	if err != nil {
		log.Fatal(err)
	}

	BulkPrint([]PathGroup{duplicates}, file)
	buf := bytes.NewBuffer(nil)

	file.Seek(0, os.SEEK_SET)
	io.Copy(buf, file)
	file.Close()

	var str strings.Builder
	str.WriteString("======== originals ========\n")
	str.WriteString("======= dir =======\n")
	str.WriteByte('\n')
	str.WriteString("dir/dir/dummy.tt\n")
	str.WriteByte('\n')
	str.WriteString("======== duplicates ========\n")
	str.WriteString("dir/dir/dummy2.tt\n")
	str.WriteString("dir/dummy.tt\n")
	str.WriteString("dir/dummy2.tt\n")

	if buf.String() != str.String() {
		t.Errorf("BulkPrint: got %s\nnot\n%s", buf.String(), str.String())
	}

	os.Remove("tmp_dump")
	removeFile()

	removeDir()
}

func TestRemoveDuplicate(t *testing.T) {
	createDir(1)
	createFile("dir/")

	var opt Options = Options{Bulk: false, Detailed: false, Autodel: true, Verbose: false, Cli: true, Output: "", Paths: []string{"."}}

	AnalyzeDir("dir", opt)

	if _, err := os.Open("dir/dummy2.tt"); err == nil {
		t.Errorf("Remove Duplicates: dummy2.tt not removed")
	}

	removeDir()
}

func TestCreateMenus(t *testing.T) {
	createDir(1)
	createFile("dir/")

	hmap := make(map[string][]TimeStampedFile)

	hmap["a"] = []TimeStampedFile{{Name: "a1", Timestamp: time.Now()}, {Name: "a2", Timestamp: time.Now()}, {Name: "a3", Timestamp: time.Now()}}
	hmap["b"] = []TimeStampedFile{{Name: "b1", Timestamp: time.Now()}, {Name: "b2", Timestamp: time.Now()}}
	hmap["c"] = []TimeStampedFile{{Name: "c1", Timestamp: time.Now()}, {Name: "c2", Timestamp: time.Now()}, {Name: "c3", Timestamp: time.Now()}, {Name: "c4", Timestamp: time.Now()}}

	pg := PathGroup{OriginPath: ".", HashMap: hmap}

	mlist := CreateMenus(pg)

	current := mlist[0]
	err := false

	for _, value := range mlist {
		current = value
		if current.NbElements != 2 || current.CursorPos != 0 || !StringEquals(current.Options, []TimeStampedFile{{Name: "b1", Timestamp: time.Now()}, {Name: "b2", Timestamp: time.Now()}}) || !BoolEquals(current.Selected, []bool{false, false}) {
			err = true
		} else {
			err = false
			break
		}
	}

	if err {
		t.Errorf("Expected Menu {nbElement: 2, cursorPos: 0, options: [b1 b2], selected: [false false]\n\ngot\n\n %+v", current)
	}

	err = false
	for _, value := range mlist {
		current = value
		if current.NbElements != 4 || current.CursorPos != 0 || !StringEquals(current.Options, []TimeStampedFile{{Name: "c1", Timestamp: time.Now()}, {Name: "c2", Timestamp: time.Now()}, {Name: "c3", Timestamp: time.Now()}, {Name: "c4", Timestamp: time.Now()}}) || !BoolEquals(current.Selected, []bool{false, false, false, false}) {
			err = true
		} else {
			err = false
			break
		}
	}

	if err {
		t.Errorf("Expected Menu {NbElement: 4, CursorPos: 0, Options: [c1 c2 c3 c4], Selected: [false false false false]\n\ngot\n\n %+v", current)
	}

	err = false
	for _, value := range mlist {
		current = value
		if current.NbElements != 3 || current.CursorPos != 0 || !StringEquals(current.Options, []TimeStampedFile{{Name: "a1", Timestamp: time.Now()}, {Name: "a2", Timestamp: time.Now()}, {Name: "a3", Timestamp: time.Now()}}) || !BoolEquals(current.Selected, []bool{false, false, false}) {
			err = true
		} else {
			err = false
			break
		}
	}

	if err {
		t.Errorf("Expected Menu {nbElement: 3, cursorPos: 0, options: [a1 a2 a3], selected: [false false false]\n\ngot\n\n %+v", current)
	}

	removeDir()
}
