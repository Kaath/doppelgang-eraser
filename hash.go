package main

import (
	"bytes"
	"crypto/sha256"
	"fmt"
	"hash"
	"io"
	"io/ioutil"
	"log"
	"os"
	"time"
)

type TimeStampedFile struct {
	Name      string
	Timestamp time.Time
}

type PathGroup struct {
	OriginPath string
	HashMap    map[string][]TimeStampedFile
}

func makeHash(file *os.File) hash.Hash {
	hash := sha256.New()
	if _, err := io.Copy(hash, file); err != nil {
		log.Fatal(err)
	}
	return hash
}

func RemoveSolos(duplicates *map[string][]TimeStampedFile) {
	for key, files := range *duplicates {
		if len(files) == 1 {
			delete(*duplicates, key)
		}
	}
}

func removeDuplicates(hmap map[string][]TimeStampedFile, opt Options) {
	for _, files := range hmap {
		for i := 1; i < len(files); i++ {
			if opt.Verbose {
				file, err := os.OpenFile(opt.Output,
					os.O_APPEND|os.O_CREATE|os.O_RDWR,
					0644)
				if err != nil {
					log.Fatal(err)
				}
				fmt.Fprintln(file, "Removed: ", files[i])
				file.Close()
			}
			os.Remove(files[i].Name)
		}
	}
}

func sortTimeStamped(array *[]TimeStampedFile) {
	notSorted := true
	for notSorted {
		notSorted = false
		for index, tsf := range *array {
			if index == len(*array)-1 {
				break
			}
			if tsf.Timestamp.After((*array)[index+1].Timestamp) {
				tmp := tsf
				(*array)[index] = (*array)[index+1]
				(*array)[index+1] = tmp
				notSorted = true
			}
		}
	}
}

func CompareFiles(first *os.File, second *os.File) bool {
	hash := sha256.New()
	if _, err := io.Copy(hash, first); err != nil {
		log.Fatal(err)
	}

	hash2 := sha256.New()
	if _, err := io.Copy(hash2, second); err != nil {
		log.Fatal(err)
	}

	return bytes.Equal(hash.Sum(nil), hash2.Sum(nil))
}

func analyzeDir(path string, opt Options) (duplicates map[string][]TimeStampedFile) {
	duplicates = make(map[string][]TimeStampedFile)
	files, err := ioutil.ReadDir(path)

	if err != nil {
		printErr(err, opt)
		return nil
	}

	for _, file := range files {
		if file.IsDir() {
			dirfiles := analyzeDir(path+"/"+file.Name(), opt)
			for key, value := range dirfiles {
				duplicates[key] = append(duplicates[key], value...)
			}
		} else if file.Mode().IsRegular() {
			file_original, err := os.Open(path + "/" + file.Name())
			if err != nil {
				printErr(err, opt)
				continue
			}
			hash := makeHash(file_original)
			str := string(hash.Sum(nil))
			if err != nil {
				printErr(err, opt)
				file_original.Close()
				continue
			}
			tsf := TimeStampedFile{file_original.Name(), file.ModTime()}
			duplicates[str] = append(duplicates[str], tsf)
			file_original.Close()
		} else {
			continue
		}

	}

	if opt.Autodel {
		removeDuplicates(duplicates, opt)
	}
	return duplicates
}

func AnalyzeDir(path string, opt Options) (duplicates map[string][]TimeStampedFile) {
	return analyzeDir(trim(path), opt)
}
